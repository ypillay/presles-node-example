// Initialise variables for express
const express = require('express')
const app = express()
const port = 5050
const bodyParser = require('body-parser')
const RECORDING_PATH = '/var/spool/asterisk/monitor/' // default asterisk recording path is /var/spool/asterisk/monitor

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

/**
 * Index Route
 * @returns {json}
 */
app.get('/', (req, res) => res.json({ data: "Hello, world!" }))

/**
 * Download specified call recording, prepends RECORDING_PATH to requested filename
 * @returns {file}
 * @param {string} filename - Path to call recording
 */
app.post('/download', (req, res) => res.download(`${RECORDING_PATH}${req.body.filename}`))

// Start server
app.listen(port, () => console.log(`App running on port ${port}`))