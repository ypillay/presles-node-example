# Pre-requisites

To successfully set this example up, you need to make sure your environment is running the latest Node LTS release.

# Setting up the example

1. Clone this repo using `git clone git@bitbucket.org:ypillay/presles-node-example.git`
2. Once the repo has been cloned, navigate to the directory, by default it will be named `presles-node-example`
3. In your terminal, run `npm install`
4. Once the above process is complete, in your terminal type `node app.js`. The server should start and will run on port 5050 by default.

# Example call to download call recording

Make a post request to `http://localhost:5050/download` using any REST client of your choosing, with the following request JSON body, making sure to change the value of the `filename` key:

```js
{
    "filename": "default/20190328/20190328-144350_1553777030.48.WAV"
}
```

If the file exists on your filesystem, it will be returned, ready for downloading (to automatically download it, you will need to adapt this to a frontend)